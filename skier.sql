-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2017 at 12:17 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

DROP SCHEMA

IF EXISTS skierdb;
	CREATE SCHEMA skierdb COLLATE = utf8_general_ci;

USE skierdb;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skier`
--

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

CREATE TABLE `club` (
  `id` varchar(12) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `City` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `County` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entry`
--

CREATE TABLE `entry` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `area` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `distance` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `skierUserName` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `seasonYear` int(4) NOT NULL,
  `entryID` int(11) NOT NULL,
  `totalDistance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `season`
--

CREATE TABLE `season` (
  `fallYear` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `seasonclub`
--

CREATE TABLE `seasonclub` (
  `seasonYear` int(4) NOT NULL,
  `clubId` varchar(12) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `fName` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `eName` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `yearOfBirth` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skierinclub`
--

CREATE TABLE `skierinclub` (
  `sUsername` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `clubId` varchar(12) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `fallYear` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entry`
--
ALTER TABLE `entry`
  ADD PRIMARY KEY (`id`,`date`,`area`) USING BTREE;
--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`skierUserName`,`seasonYear`,`entryID`),
  ADD KEY `seasonYear` (`seasonYear`),
  ADD KEY `entryId` (`entryID`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`fallYear`);

--
-- Indexes for table `seasonclub`
--
ALTER TABLE `seasonclub`
  ADD PRIMARY KEY (`seasonYear`,`clubId`),
  ADD KEY `clubIdid` (`clubId`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `skierinclub`
--
ALTER TABLE `skierinclub`
  ADD PRIMARY KEY (`sUsername`,`clubId`, `fallYear`),
  ADD KEY `club` (`clubId`),
  ADD KEY `fallYear` (`fallYear`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `entry`
--
ALTER TABLE `entry`
  MODIFY `id` int(11) NOT NULL,
  ADD CONSTRAINT `entryId` FOREIGN KEY (`id`) REFERENCES `log` (`entryID`);
--
-- Constraints for dumped tables
--

--
-- Constraints for table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `seasonYear` FOREIGN KEY (`seasonYear`) REFERENCES `season` (`fallYear`),
  ADD CONSTRAINT `username` FOREIGN KEY (`skierUserName`) REFERENCES `skier` (`userName`);

--
-- Constraints for table `seasonclub`
--
ALTER TABLE `seasonclub`
  ADD CONSTRAINT `clubIdid` FOREIGN KEY (`clubId`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `sYear` FOREIGN KEY (`seasonYear`) REFERENCES `season` (`fallYear`);

--
-- Constraints for table `skierinclub`
--
ALTER TABLE `skierinclub`
  ADD CONSTRAINT `club` FOREIGN KEY (`clubId`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `skier` FOREIGN KEY (`sUsername`) REFERENCES `skier` (`userName`),
  ADD CONSTRAINT `year` FOREIGN KEY (`fallYear`) REFERENCES `season` (`fallYear`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
