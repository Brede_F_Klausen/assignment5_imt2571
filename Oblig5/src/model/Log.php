<?php
class Log {
    //public $logIndex;
    public $entryID;
    public $clubId;
    public $userName;
    public $season;
	public $totalDistance;
	public $entryCount;
	public $entries;

	public function __construct($season = "", $entryID)
    {
        $this->entryID = $entryID;
        $this->season = $season;
    } 
}
?>