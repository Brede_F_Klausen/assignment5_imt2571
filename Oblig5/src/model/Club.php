<?php
class Club {
	public $id;
	public $name;
	public $city;
	public $county;

	public function __construct($id = "", $name = "", $city = "", $county = "")
    {  
        $this->id = $id;
		$this->name = $name;
        $this->city = $city;
	    $this->county = $county;
    }
}
?>