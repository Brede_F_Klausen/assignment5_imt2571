<?php
class Entry {
	public $area;
	public $distance;
	public $eDate;

	public function __construct($area = "", $distance = "", $eDate = "")
    {
		$this->area = $area;
        $this->distance = $distance;
	    $this->eDate = $eDate;
    } 
}
?>