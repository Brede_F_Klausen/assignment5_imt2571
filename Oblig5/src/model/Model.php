<?php
// Sources:
// Creating DOMDocument and getting from file
// https://stackoverflow.com/questions/194574/inserting-data-in-xml-file-with-php-dom


include_once("Skier.php");
include_once("Club.php");
include_once("Entry.php");
include_once("Season.php");
include_once("Log.php");
include_once("IModel.php");
include_once("SeasonClub.php");


class Model implements IModel
{
    public $clubs;
    public $skiers;
    public $logs;
    public $season;
    public $seasonClub;
    public $yearCount;
    public $entryCount;
    public $seasonClubCount;
	public function __construct()
    {
        // TODO: Classes might need to keep their own count.
        $this->seasonClubCount = 0;
        $this->entryCount = 0;
        $rawDOMFromXML = $this->getXMLData();

        $xpath = New DOMXPath($rawDOMFromXML);

        $this->clubs = $this->getAllClubs($xpath);
        $this->skiers = $this->getAllSkiers($xpath);

        $lastFallYear = false;
        $yearLoopCount = 0;
        while (!$lastFallYear){

        	$year = 2015 + $yearLoopCount;
        	$qryString = "//SkierLogs/Season[@fallYear=$year]";
            $seasonQry = $xpath->query($qryString);
        	if($seasonQry->length == 0){
                $lastFallYear = true;
			}
			else {
                $this->season[$yearLoopCount] = New Season($year);
                $this->logs[$yearLoopCount] = $this->getAllLogs($xpath, $seasonQry, $year);
                $yearLoopCount++;
            }
		}
		$this->yearCount = $yearLoopCount;
    }

    public function getAllClubs(DOMXPath &$xpath){
        $clubsQry = $xpath->query('//SkierLogs/Clubs/Club');
        $clubsLength = $clubsQry->length;
        for ($i = 0; $i < $clubsLength; $i++){
            $clubObjects[$i] = New Club();

            $club = $clubsQry->item($i);
            $clubsId = $club->getAttribute('id');
            $clubName = $club->getElementsByTagName("Name");
            $clubCity = $club->getElementsByTagName("City");
            $clubCounty = $club->getElementsByTagName("County");

            $clubObjects[$i]->id = $clubsId;
            $clubObjects[$i]->name = $clubName[0]->nodeValue;
            $clubObjects[$i]->city = $clubCity[0]->nodeValue;
            $clubObjects[$i]->county = $clubCounty[0]->nodeValue;
        }

        return (array)$clubObjects;
    }

    public function getAllSkiers(DOMXPath &$xpath){
        $skiersQry = $xpath->query('//SkierLogs/Skiers/Skier');
        $skiersLength = $skiersQry->length;
        for ($i = 0; $i < $skiersLength; $i++){
            $skierObjects[$i] = New Skier();

            $skier = $skiersQry->item($i);
            $skierUsername = $skier->getAttribute('userName');
            $skierFName = $skier->getElementsByTagName("FirstName");
            $skierLName = $skier->getElementsByTagName("LastName");
            $skierYoB = $skier->getElementsByTagName("YearOfBirth");

            $skierObjects[$i]->userName = $skierUsername;
            $skierObjects[$i]->fName = $skierFName[0]->nodeValue;
            $skierObjects[$i]->lName = $skierLName[0]->nodeValue;
            $skierObjects[$i]->yearOfBirth = $skierYoB[0]->nodeValue;
        }
        return (array)$skierObjects;
    }
    public function getAllEntry(DOMXPath $xpath, string $qryString, string $clubID, string $username, Log &$seasonLog){
        $totalDistance = 0;
        $entries = $xpath->query
        ("$qryString/Skiers[@clubId='$clubID']/Skier[@userName='$username']/Log/Entry");
        $entriesCount = $entries->length;

        for ($i = 0; $i < $entriesCount; $i++) {
            $entry = $entries->item($i);

            $skierEntry[$i] = New Entry;
            $entryDate = $entry->getElementsByTagName("Date");
            $entryArea = $entry->getElementsByTagName("Area");
            $entryDistance = $entry->getElementsByTagName("Distance");

            $skierEntry[$i]->date = $entryDate[0]->nodeValue;
            $skierEntry[$i]->area = $entryArea[0]->nodeValue;
            $skierEntry[$i]->distance = $entryDistance[0]->nodeValue;
            $totalDistance += $skierEntry[$i]->distance;
            $seasonLog->entries[$i] = $skierEntry[$i];
        }
        $seasonLog->totalDistance = $totalDistance;
        return $entriesCount;
    }

    public function getAllLogs(DOMXPath $xpath, DOMNodeList $seasonQry, $year){
        $seasonItem = $seasonQry->item(0);
        $skiers = $seasonItem->getElementsByTagName("Skiers");
        $qryString = "//SkierLogs/Season[@fallYear=$year]";

        $skierLoopCount = 0;
        foreach ($skiers as $club){
            $clubID = $club->getAttribute("clubId");
            if($clubID) {
                $this->seasonClub[$this->seasonClubCount] = New SeasonClub();
                $this->seasonClub[$this->seasonClubCount]->clubID = $clubID;
                $this->seasonClub[$this->seasonClubCount++]->season = $year;
                $clubSkier = $club->getElementsByTagName("Skier");
                foreach ($clubSkier as $skier) {

                    $seasonLog[$skierLoopCount] = New Log("$year", $this->entryCount);
                    $username = $skier->getAttribute("userName");
                    $seasonLog[$skierLoopCount]->clubId = $clubID;
                    $seasonLog[$skierLoopCount]->userName = $username;

                    $entriesCount = $this->getAllEntry

                    ($xpath, $qryString, $clubID, $username, $seasonLog[$skierLoopCount]);

                    $seasonLog[$skierLoopCount]->entryCount = $entriesCount;
                    $this->entryCount++;
                    $skierLoopCount++;
                }
            }
        }
        return $seasonLog;
    }

    public function rainbowPrint(string $boringString = ""){
        $r = rand(60,255);
        $g = rand(60,255);
        $b = rand(60,255);
        print("<p style='color:rgb($r,$g,$b);'><b>$boringString</b></p>");
    }

    public function getXMLData(){
		$XMLData = New DOMDocument;
		if(!$XMLData->load('SkierLogs.xml', LIBXML_COMPACT)){
			return New ErrorException("Failed to retrieve XML content from SkierLogs", 500);
		}
		return $XMLData;
	}

	public function createNewDOM(XMLReader $xmlread)
    {
        // TODO: Implement createNewDOM() method.
    }

    public function analyzeAndSplitDOM(DOMDocument $firstIteration){
		//  TODO:
	}

	public function insertToSQLDB(){
		// TODO:
	}


}

?>
