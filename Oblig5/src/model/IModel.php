<?php
Interface IModel {
  /** Function for creating DOMDocument with XML
   * @return DOMDocument
   * @throws ErrorException // TODO: not sure what this will return
   */
  public function createNewDOM(XMLReader $xmlread);
  /** Function for analyzing and splitting DOMDocument into logical chunk
   *  based on the skierDB
   * @return DOMDocument[]
   * @throws ErrorException
   */
  public function analyzeAndSplitDOM(DOMDocument $firstIteration);
  /** Function for analyzing and splitting DOMDocument into logical chunk
   *  based on the skierDB
   * @return DOMDocument[]
   * @throws ErrorException
   */
  //public function analyzeAndRelateDOM([]DOMDocument $secondIteration);
  /** Call all class insert in correct sequence
   *  based on the skierDB
   * @return DOMDocument[]
   * @throws ErrorException
   */

   // TODO: This should be split into the classes
  public function insertToSQLDB();
}

?>
