<?php
include_once("Skier.php");
include_once("Entry.php");
include_once("Season.php");
include_once("Log.php");
include_once("Club.php");
include_once("Model.php");

class sqlDB extends model
{
    protected $db = null;

    public function __construct($db = null)
    {
        parent::__construct();
        if ($db) {
            $this->db = $db;
        } else {
            try {
                $this->db = new PDO('mysql:host=localhost;dbname=skierdb;charset=utf8mb4', 'root', '');
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            } catch (PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();

            }
        }
        $this->runSQL();
    }

    public function runSQL()
    {
        try{
            foreach ($this->season as $season) {
                $this->addSeason($season);
            }

            foreach ($this->skiers as $skier) {
                $this->addSkier($skier);
            }
            foreach ($this->clubs as $club) {
                $this->addClub($club);
            }

            foreach($this->logs as $logArray){
                foreach($logArray as $log) {
                    $this->addLog($log);
                    $this->addSkierInClub($log);
                }
            }

            foreach($this->seasonClub as $seasonClub){
                $this->addSeasonClub($seasonClub);
            }

        }
        catch(PDOException $exception){
            print($exception->getMessage());
        }
	}

	// TODO: Endre eName til lName i SQL og i kode
    public function addSkier($skier)
    {
        $yearInt = (int)$skier->yearOfBirth;
        try {
            $prep = $this->db->prepare("INSERT INTO skier(userName, fName, eName, yearOfBirth) VALUES(:userName, :fName, :eName, :yearOfBirth)");
            $prep->bindParam(':userName', $skier->userName, PDO::PARAM_STR, 60);
            $prep->bindParam(':fName', $skier->fName, PDO::PARAM_STR, 60);
            $prep->bindParam(':eName', $skier->lName, PDO::PARAM_STR, 60);
            $prep->bindParam(':yearOfBirth', $yearInt, PDO::PARAM_INT, 4);
            $prep->execute();
        }
        catch (PDOException $exception){
            throw $exception;
        }
    }

    public function addClub($club)
    {
        try {
            $prep = $this->db->prepare("INSERT INTO club(id, name, City, County) VALUES(:id, :name, :City, :County)");
            $prep->bindParam(':id', $club->id, PDO::PARAM_STR, 12);
            $prep->bindParam(':name', $club->name, PDO::PARAM_STR, 60);
            $prep->bindParam(':City', $club->city, PDO::PARAM_STR, 60);
            $prep->bindParam(':County', $club->county, PDO::PARAM_STR, 60);
            $prep->execute();
        }
        catch (PDOException $exception){
            throw $exception;
        }

    }

    public function addEntry($entry, $entryID)
    {
        try {
            $prep = $this->db->prepare("INSERT INTO entry(id, area, distance, date) VALUES(:id, :area, :distance, :date)");
            $prep->bindParam(':id', $entryID, PDO::PARAM_INT, 11);
            $prep->bindParam(':area', $entry->area, PDO::PARAM_STR, 60);
            $prep->bindParam(':distance', $entry->distance, PDO::PARAM_STR, 60);
            $prep->bindParam(':date', $entry->date, PDO::PARAM_STR, 60);
            $prep->execute();
        }
        catch(PDOException $exception){
            throw $exception;
        }
    }

    //todo fiks datatype seasonYear
    public function addLog($log)
    {
        try {
            $prep = $this->db->prepare("INSERT INTO log(skierUserName, seasonYear, entryID, totalDistance) VALUES(:skierUserName, :seasonYear, :entryID, :totalDistance)");
            $prep->bindParam(':skierUserName', $log->userName, PDO::PARAM_STR, 60);
            $prep->bindParam(':seasonYear', $log->season, PDO::PARAM_INT, 4);
            $prep->bindParam(':entryID', $log->entryID, PDO::PARAM_INT, 11);
            $prep->bindParam(':totalDistance', $log->totalDistance, PDO::PARAM_INT, 11);
            $prep->execute();
            foreach ($log->entries as $entry) {
                $this->addEntry($entry, $log->entryID);
            }
        }
        catch (PDOException $exception){
            throw $exception;
        }

    }

    public function addSkierInClub($log)
    {
        try {
            $prep = $this->db->prepare("INSERT INTO skierinclub(sUsername, clubID, fallYear) VALUES(:sUsername, :clubID, :fallYear)");
            $prep->bindParam(':sUsername', $log->userName, PDO::PARAM_STR, 60);
            $prep->bindParam(':clubID', $log->clubId, PDO::PARAM_STR, 12);
            $prep->bindParam(':fallYear', $log->season, PDO::PARAM_INT, 4);
            $prep->execute();
        }
        catch(PDOException $exception){
            throw $exception;
        }
    }

    public function addSeason($season)
    {
        try {
            $prep = $this->db->prepare("INSERT INTO season(fallYear) VALUES(:fallYear)");
            $prep->bindParam(':fallYear', $season->fallYear, PDO::PARAM_INT, 4);
            $prep->execute();
        }
        catch(PDOException $exception){
            throw $exception;
        }
    }

    public function addSeasonClub($seasonClub){
        try{
            $prep = $this->db->prepare("INSERT INTO seasonClub(seasonYear, clubId) VALUES(:seasonYear, :clubId) ");
            $prep->bindParam(':seasonYear', $seasonClub->season, PDO::PARAM_INT, 4);
            $prep->bindParam(':clubId', $seasonClub->clubID, PDO::PARAM_STR, 12);
            $prep->execute();
        }
        catch(PDOException $exception){
            throw $exception;
        }
    }
}

