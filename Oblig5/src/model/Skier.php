<?php
class Skier {
	public $userName;
	public $fName;
	public $lName;
	public $yearOfBirth;

	public function __construct($userName = "", $fName = "", $lName = "", $yearOfBirth = "")
    {  
        $this->userName = $userName;
        $this->fName = $fName;
	    $this->lName = $lName;
	    $this->yearOfBirth = $yearOfBirth;
    } 
}
?>