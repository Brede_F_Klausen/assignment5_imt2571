<?php
/**
 * Created by PhpStorm.
 * User: halla
 * Date: 06/11/2017
 * Time: 01:02
 */

class SeasonClub
{
    public $season;
    public $clubID;

    public function __construct($season = "", $clubID = "")
    {
        $this->season = $season;
        $this->clubID = $clubID;
    }
}