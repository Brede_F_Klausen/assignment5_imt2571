# README #

### This Repo is under The GNU General Public License

## Collaborators ##

| Name                  | Bitbucket                                                 | Email                                                 |
|-----------------------|-----------------------------------------------------------|-------------------------------------------------------|
| Aksel Hjerpbakk       | [Avokadoen](https://bitbucket.org/Avokadoen/)             | [akselhj@stud.ntnu.no](mailto:akselhj@stud.ntnu.no)   |
| Brede Fritjof Klausen | [Brede_F_Klausen](https://bitbucket.org/Brede_F_Klausen/) | [bredefk@stud.ntnu.no](mailto:bredefk@stud.ntnu.no)   |
| Mathias Stifjeld      | [Geoduck](https://bitbucket.org/Geoduck/)                 | [mathisti@stud.ntnu.no](mailto:mathisti@stud.ntnu.no) |
| Sindre �strem	        | [Sindreoestrem](https://bitbucket.org/Sindreoestrem/)     | [sindrost@stud.ntnu.no](mailto:sindrost@stud.ntnu.no) |